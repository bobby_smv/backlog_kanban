// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'global-components'

import 'va/lib/css'
import 'va/lib/script'

import App from './App'
import router from './router'

import jQuery from 'jquery'

global.jquery = jQuery
global.$ = jQuery
window.$ = window.jQuery = jQuery
Vue.prototype.$ = jQuery;

import 'jquery-ui/ui/widgets/draggable'
import 'jquery-ui/ui/widgets/droppable'

// import jQueryUI from 'jquery-ui'
// window.$ = window.$.extend(jQueryUI);

Vue.config.productionTip = false

/* eslint-disable no-new */
window.vue = new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
})
