import Vue from 'vue';

import Modal from 'components/common/bootstrap-modal';
import WindowAlert from 'components/common/window-alert';
import PageLoading from 'components/common/page-loading';
import InputRadio from 'components/common/input-radio';
import VueSelectbox from 'components/common/v-selectbox';
import ImgAvatar from 'components/common/img-avatar';
import InputCheckbox from 'components/common/input-checkbox';
import InputGroup from 'components/common/bootstrap-input-group';
import BtnGroup from 'components/common/bootstrap-btn-group';
import Alert from 'components/common/bootstrap-alert';
import Tooltip from 'components/common/tooltip';
import Dateset from 'components/common/dateset';
import DatesetRange from 'components/common/dateset-range';

// global components
Vue.component('modal', Modal);
Vue.component('window-alert', WindowAlert);
Vue.component('page-loading', PageLoading);
Vue.component('input-radio', InputRadio);
Vue.component('v-selectbox', VueSelectbox);
Vue.component('img-avatar', ImgAvatar);
Vue.component('input-checkbox', InputCheckbox);
Vue.component('input-group', InputGroup);
Vue.component('btn-group', BtnGroup);
Vue.component('alert', Alert);
Vue.component('tooltip', Tooltip);
Vue.component('dateset', Dateset);
Vue.component('dateset-range', DatesetRange);

// plugin
import Select2 from 'v-select2-component';
Vue.component('select2', Select2);
