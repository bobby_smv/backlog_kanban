import _countBy from 'lodash/countBy';


/**
 * デバッグ用
 * @param   {object}   obj
 */
export function consoleJsonStringify(obj) {
    try {
        if ( !!obj && (typeof obj === 'object') ) {
            console.log( JSON.stringify(obj, null, '\t') );
        } else {
            console.log( obj );
        }
    } catch(error) {
        // no action
    }
}

/**
 * スクロールY値を返す
 * @return  {number}
 */
export function getScrollY(target) {
    if ( typeof target !== 'undefined' || (window === target) ) {
        return window.pageYOffset || window.scrollY;
    } else if (typeof target !== 'string') {
        let elm = document.querySelector(target);
        return !!elm ? elm.scrollTop : 0;
    } else {
        return ('scrollTop' in target) ? target.scrollTop : 0;
    }
}

/**
 * 一意な値を生成
 * @return  {number}
 */
export function getRandNumber() {
    return Math.random() * 0x80000000 | 0;
}

/**
 * 3桁カンマを挿入して返す
 */
export function getNumberFormat(value) {
    if ( Number.isNaN(value - 0) ) {
        return value;
    }
    var str = value + ''; //{string}

    while( str != (str = str.replace(/^(-?\d+)(\d{3})/, "$1,$2")) ); //3桁毎にカンマを挿入

    return str;
}

/**
 * パーセント変換した値（単位なし）を返す
 * @param  {number}  value
 * @param  {number}  digits - 小数点何位まで
 */
export function toPercentValue(value, digits) {
    digits = (typeof digits === 'undefined') ? 0 : digits;

    let nums = (value + '').split('.');

    if (nums.length < 2) {
        return value * 100; // 小数点なし
    }

    let intValue = nums[0] + (nums[1] + '00').substring(0, 2);
    let decimalValue = nums[1].substring(2, (digits + 2));

    if (digits < 1) {
        // int
        return Number(intValue);
    } else {
        // float
        return Number(intValue + '.' + decimalValue);
    }
}

/**
 * @param   {number}   value
 * @param   {string}   unit
 * @param   {*}
 */
export function getFileSize(value, unit) {
    unit = (typeof unit === 'undefined') ? 'kb' : unit;
    value = value - 0; //数値化

    switch (unit) {
        case 'kb':
            value = value / 1000;
            break;
        case 'mb':
            value = value / 1000 / 1000;
            break;
    }
    value = value * 1000; // 小数点以下3桁を整数部分にする
    value = Math.round(value); // 小数点以下四捨五入

    return value / 1000 + unit.toUpperCase(); // 小数点の位置を元に戻す
}

/**
 * @param   {number}   value
 * @param   {number}   digits
 * @param   {*}
 */
export function getZeroPadding(value, digits) {
    digits = (typeof digits === 'undefined') ? 2 : digits;

    // 空または数値にできない場合はそのまま返す
    if (value === '') {
        return value;
    } else if ( Number.isNaN(value - 0) ) {
        return value;
    }

    value = value - 0; //数値化

    var zero = (function(d) {
        var rtn = '';
        for (var i = 0; i < (d - 1); ++i) {
            rtn += '0';
        }
        return rtn;
    })(digits);

    var overNum = ('1' + zero) - 0;

    return (value < overNum) ? (zero + value) : ('' + value);
}

/**
 * @param  {number}  index
 */
export function setCursorIndex(el, index) {
    var isModernBrowser = ('selectionStart' in el) && ('selectionEnd' in el);
    var range;

    if (isModernBrowser) {
        el.selectionStart = index;
        el.selectionEnd   = index;
    } else {
        range = document.selection.createRange();
        range.moveStart('character', index);
        range.moveEnd('character', 0);
        range.select();
    }
}

/**
 * @param  {string}  text
 * @param  {number}  index
 * @param  {string}  insertText
 * @return {string}
 */
export function getInsertString(text, index, insertText) {
    var before = text.substring(0, index);
    var after  = text.substring(index, text.length);

    return before + insertText + after;
}

/**
 * @return {number}
 */
export function getCursorIndex(el) {
    var isModernBrowser = ('selectionStart' in el) && ('selectionEnd' in el);
    var range;

    if (isModernBrowser) {
        return el.selectionStart;
    } else {
        el.focus();
        range = document.selection.createRange();
        range.moveStart('character', -el.value.length);
        return range.text.length;
    }
}

/**
 * EventListener DOMContentLoaded
 * DOMLoadが完了している場合は、callbackを即時実行する
 * @param   {function}   callback
 */
export function bindDOMContentLoaded(callback) {
    if (typeof callback !== 'function') return;

    if ('loading' !== document.readyState) {
        callback();
    } else {
        document.addEventListener('DOMContentLoaded', callback, false);
    }
}

/**
 * リサイズが完了したらcallback実行
 * @param   {function}   resized
 * @param   {function}   resizeing
 * @param   {number}     delay
 */
export function bindFixedResize(resized, resizeing, delay) {
    delay = delay || 150;

    var timer = false;

    var _cb = function(e) {
        if (false !== timer) {
            if (typeof resizeing === 'function') {
                resizeing(e);
            }
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            if (typeof resized === 'function') {
                resized(e);
            }
        }, delay);
    };

    window.addEventListener('resize', _cb, false);

    return _cb;
}

/**
 * URLパラメータを削除したURLを返す
 * @return {string}
 */
export function getRemoveQueryUrl(url) {
    url = !!url ? (url + '') : window.location.href;

    let split = url.split('?');

    return (split.length > 1) ? split[0] : getRemoveVueRouterPath(url);
}

/**
 * URLの末尾がvue-routerハッシュの場合は、削除したURLを返す
 * @param  {string}  url
 * @return {string} - remove vue-router hash
 */
export function getRemoveVueRouterPath(url) {
    url = url || window.location.href;

    var index = url.lastIndexOf('#/');

    if ( index === (url.length - 2) ) {
        return url.substr(0, index);
    } else {
        return url;
    }
}


/**
 * URLのパラメータ情報を返す。パラメータ名がキー、GET値が値
 * http://www.crystal-creation.com/web-appli/technical-information/programming/javascript/sample/get.htm
 * @param   {string}   url
 * @return  {object}
 */
export function getUrlQueries(url){
    url = getRemoveVueRouterPath(url || window.location.href);

    var result = {};

    var search = (function() {
        var urlIndex = url.indexOf('?');
        var urlLen   = url.length;

        if ( url && (urlIndex > -1) ) {
            return url.substring(urlIndex, urlLen) || '';
        } else if (url) {
            return '';
        } else {
            return window.location.search;
        }
    })();

    if ( 1 < search.length ) {
        var query      = search.substring( 1 );
        var parameters = query.split( '&' );

        for( var i = 0; i < parameters.length; i++ ) {
            var elm = parameters[ i ].split( '=' );
            var paramName  = decodeURIComponent( elm[ 0 ] );
            var paramValue = decodeURIComponent( elm[ 1 ] );
            result[ paramName ] = (paramValue === 'undefined') ? '' : paramValue;
        }
    }
    return result;
}

/**
 * URLパラメータの文字列を返す
 * @param   {object}   queries
 * @return  {string}
 */
export function getUrlQueryString(queries) {
    if ( !queries || (typeof queries !== 'object') ) {
        return '';
    }
    let queryString = '';
    let i = 0;

    for (let key in queries) {
        let pref = (i === 0) ? '?' : '&';
        if (typeof queries[key] !== 'undefined') {
            queryString += pref + key + '=' + queries[key];
        }
        i++;
    }

    return queryString;
}

/**
 * URLパラメータを付与したURLを返す
 * @param   {object}   queries
 * @return  {string}
 */
export function getAddQueryUrl(queries, url) {
    url = url || window.location.href;

    if ( !queries || (typeof queries !== 'object') ) {
        return url;
    }

    let noQueryUrl = getRemoveQueryUrl(url);
    let urlQueries = Object.assign(getUrlQueries(url), queries);

    return noQueryUrl + getUrlQueryString(urlQueries);
}

export function toAlphabet(index) {
    return String.fromCharCode(index + 65)
}

/**
 * https://css-tricks.com/snippets/javascript/htmlentities-for-javascript/
 * @param  {string}  str
 */
export function getHtmlEntities(str) {
    str = str + '';
    return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/**
 * 改行コードを返す
 * http://www.geocities.co.jp/SiliconValley/1715/getCRChars.html
 * @return   {string}
 */
export function getLineFeedCode() {
    let code = '' + getLineFeedCode;

    if ( code.indexOf('\r\n') > -1 ) {
        return '\r\n';
    } else if ( code.indexOf('\r') > -1 ) {
        return '\r';
    } else {
        return '\n';
    }
};

/**
 * 改行コードを返す
 * http://www.geocities.co.jp/SiliconValley/1715/getCRChars.html
 * @return   {string}
 */
export function removeLineFeedCode(str) {
    str = str.replace(/\r\n/g, '');
    str = str.replace(/\r/g, '');
    return str.replace(/\n/g, '');
}

/**
 * select[selected]のvalue値を返す
 * @param   {HTMLElement}   selectbox
 * @return  {*}
 */
export function getSelectedValue(selectbox) {
    try {
        return selectbox.options[selectbox.selectedIndex].value;
    } catch(error) {
        return null;
    }
};

/**
 * 頭1文字を大文字にした文字列を返す
 * @param   {string}   value
 * @return  {string}
 */
export function ucfirst(value) {
    value = value + '';
    return value.charAt(0).toUpperCase() + value.slice(1);
}

/**
 * @param   {object} e - event object
 * @return  {boolean}
 */
export function isCmdOrCtrl(e) {
    return isMacOS() ? e.metaKey : e.ctrlKey;
}

/**
 * @return  {bool}
 */
export function validateRequired(value) {
    return Array.isArray(value)
                ? (value.length > 0) && (value[0] !== '')
                : ((value + '') !== '');
}

/**
 * @return  {bool}
 */
export function validateMaxlength(value, max) {
    return (value + '').length <= max;
}

/**
 * @return  {bool}
 */
export function validateMinlength(value, min) {
    return (value + '').length >= min;
}

/**
 * @return  {bool}
 */
export function validatePost(value, isHyphen) {
    return isHyphen
                ? /^[0-9]{3}-[0-9]{4}$/.test(value + '')
                : /^[0-9]{7}$/.test(value + '');
}

/**
 * @return  {bool}
 */
export function validateTel(value, isHyphen) {
    return isHyphen
                ? /^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/.test(value + '')
                : /^[0-9]{10,11}$/.test(value + '');
}

/**
 * @return  {bool}
 */
export function validateEmail(value) {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value + '');
}

/**
 * 半角英数字か
 * @return  {bool}
 */
export function validateAlphanum(value) {
    return /^([a-zA-Z0-9]+)$/.test(value + '');
}

/**
 * 半角英字か
 * @return  {bool}
 */
export function validateAlphabet(value) {
    return /^([a-zA-z\s]+)$/.test(value + '');
}

/**
 * 半角数字か
 * @return  {bool}
 */
export function validateNumber(value) {
    return /^([0-9.-]+)$/.test(value + '');
}

/**
 * @return  {bool}
 */
export function validateMaxnum(value, max) {
    return parseFloat(value) <= max;
}

/**
 * @return  {bool}
 */
export function validateMinnum(value, min) {
    return parseFloat(value) >= min;
}

/**
 * 全角ひらがな・カタカナのみか
 * @return  {bool}
 */
export function validateKana(value) {
    return /^([ァ-ヶーぁ-ん]+)$/.test(value + '');
}

/**
 * URLか
 * @return  {bool}
 */
export function validateUrl(value) {
    return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(value + '');
}

/**
 * 重複値があるか
 * @param  {array<string>}  values
 * @return {object}
 *         {boolean}    - isValid
 *         {array|null} - inValidValues
 */
export function validateDuplication(values) {
    let valid = {
        isValid: true,
        inValidValues: null
    };

    if ( !Array.isArray(values) || (values.length < 2) ) {
        return valid; // 配列でない、比較対象がない
    }

    let result = _countBy(values);

    let inValidValues = [];

    for (let key in result) {
        let count = result[key] - 0;
        if ( (count > 1) && ('' !== key) ) {
            // 重複
            valid.isValid = false;
            inValidValues.push(key);
        }
    }

    if (!valid.isValid) {
        valid.inValidValues = inValidValues;
    }

    result = null;

    return valid;
}

/**
 * resolve: granted, default
 * reject: denied, Notification not support
 * @return  {Promise}
 */
export function initNotification() {
    return new Promise((resolve, reject)=> {
        function createError(message, permission) {
            let err = new Error(message);
            err.permission = permission;
            return err;
        }

        if ( !('Notification' in window) ) {
            reject( createError('Notification is not support.', 'not_support') );
            return;
        }

        let state = Notification.permission;

        switch (state) {
            case 'granted':
                // 通知許可済み
                resolve('granted');
                break;
            case 'denied':
                // ブロック中
                reject( createError('Notification state is denied.', 'denied') );
                break;
            default:
                // 通知許可確認
                Notification.requestPermission((permission)=> {
                    if (permission !== 'denied') {
                        resolve(permission);
                    } else {
                        reject( createError('Notification state is denied.', 'denied') );
                    }
                });
                break;
        }
    });
}

/**
 * @param  {string}  address - 住所
 */
export function createGoogleMapUrl(address) {
    let googleMap = 'https://www.google.com/maps';

    if (!address) return googleMap;

    address = encodeURIComponent(address);

    return `${googleMap}/search/${address}`;
}

/**
 * @param  {HTMLElement}  parent
 * @param  {HTMLElement}  base
 * @return {object}
 */
export function getRelativeOffset(parent, base) {
    let offset = {left: 0, top: 0};

    if (!parent || !base) {
        return offset;
    }

    let parentRect = parent.getBoundingClientRect();
    let baseRect   = base.getBoundingClientRect();
    console.log(parentRect);
    console.log(baseRect);

    offset.left = Math.ceil(baseRect.left - parentRect.left);
    offset.top  = Math.ceil(baseRect.top - parentRect.top);

    return offset;
}
