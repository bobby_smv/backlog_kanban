import * as types from '../mutation-types'

const state = {
    data: null
}

const mutations = {
    [types.FETCH_MYSELF] (state, data) {
        state.data = data
    }
}

export default {
    state,
    mutations
}
