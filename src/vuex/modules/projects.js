import * as types from '../mutation-types'

const state = {
    items: []
}

const mutations = {
    [types.FETCH_PROJECTS] (state, projects) {
        state.items = projects
    }
}

export default {
    state,
    mutations
}
