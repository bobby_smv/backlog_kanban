import * as types from '../mutation-types'

const state = {
    project_members: {}
}

const mutations = {

    [types.FETCH_PROJECT_MEMBERS] (state, { projectId, items }) {
        state.project_members[projectId] = items;
    }
}

export default {
    state,
    mutations
}
