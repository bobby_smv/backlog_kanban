import * as types from '../mutation-types'

const state = {

    spaceName: "sonicmoov",
    apiKey: null,

    activeProjectIds: [],
    activeMemberIds: [],

    hiddenProjectIds: [],

    filteredMemberIds: [],
    filteredVersionIds: [],
    filteredIssueTypeIds: [],
}

const mutations = {
    [types.FETCH_WORKSPACE] (state, data) {
        for (let k in data) state[k]=data[k];
    },
    [types.UPDATE_SPACE_NAME] (state, value) {
        state.spaceName = value
        this.dispatch("saveWorkspace")
    },
    [types.UPDATE_API_KEY] (state, value) {
        state.apiKey = value
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_ACTIVE_PROJECTS] (state, ids) {
        state.activeProjectIds = ids.slice()
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_ACTIVE_MEMBERS] (state, ids) {
        state.activeMemberIds = ids.slice()
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_HIDDEN_PROJECTS] (state, ids) {
        state.hiddenProjectIds = ids.slice()
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_FILTERED_MEMBERS] (state, ids) {
        state.filteredMemberIds = ids.slice()
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_FILTERED_VERSIONS] (state, ids) {
        state.filteredVersionIds = ids.slice()
        this.dispatch("saveWorkspace")
    },
    [types.SELECT_FILTERED_ISSUE_TYPES] (state, ids) {
        state.filteredIssueTypeIds = ids.slice()
        this.dispatch("saveWorkspace")
    }
}

export default {
    state,
    mutations
}
