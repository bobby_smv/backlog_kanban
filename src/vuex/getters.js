
export const apiKey = state => state.workspace.apiKey
export const spaceName = state => state.workspace.spaceName
export const endpoint = state => `https://${state.workspace.spaceName}.backlog.jp/api/v2/`


export const myself = state => state.myself.data
export const projects = state => state.projects.items
export const getProjectMembers = state => (id)=>{
    return state.members.project_members[id]
}
export const workspace = state => state.workspace

export const getActiveProjectIds = state => ()=>state.workspace.activeProjectIds.slice()
export const getActiveMemberIds = state => ()=>state.workspace.activeMemberIds.slice()
export const getHiddenProjectIds = state => ()=>state.workspace.hiddenProjectIds.slice()
export const getFilteredMemberIds = state => ()=>state.workspace.filteredMemberIds.slice()
export const getFilteredVersionIds = state => ()=>state.workspace.filteredVersionIds.slice()
export const getFilteredIssueTypeIds = state => ()=>state.workspace.filteredIssueTypeIds.slice()

export const getActiveProjectMembers = state => (id)=>{

    let list = {};
    const ids = state.workspace.activeProjectIds.slice();
    for( let id of ids )
        for( let item of (state.members.project_members[id] || []) )
            list[item.id] = item;

    return Object.values(list);
}
