import * as types from './mutation-types'


const cachePool = {};
/**
 * backlogAPIとの通信を行う。
 * @see https://developer.mozilla.org/ja/docs/Web/API/Fetch_API/Using_Fetch
 *
 * @param commit
 * @param getters
 * @param url
 * @param [params] { k:v ... }
 * @param [queryString] 'k=v&...'
 * @param [options]
 * @param {boolean} [cache]
 * @return {*}
 */
export const fetchBacklogAPI = ({ commit, getters }, {url, params = {}, queryString = '', options={}, cache = false} ) => {

    // filter
    if(!getters.apiKey)
        return Promise.reject("apiKey not found.")

    params = params||{};
    queryString = queryString || '';
    params.apiKey = getters.apiKey;
    queryString += (queryString.length>0 ? '&' : '') + Object.keys(params).map((v)=>v+'='+params[v]).join('&');

    url = `https://${getters.spaceName}.backlog.jp/api/v2/${url}?${queryString}`;

    // use cache
    if( cache && cachePool[url] ) return cachePool[url];

    return fetch( url, options ).then((response)=>{
        return response.json()
            .then(( value )=>{
                // save cache
                if( cache ) cachePool[url] = value;

                return Promise.resolve(value)
            })
    })
}

/**
 *
 * @param commit
 * @return {Promise<T | never>}
 */
export const fetchMyself = ({ commit,dispatch }) => {
    // API request
    return dispatch('fetchBacklogAPI', {url: 'users/myself', cache:true})
        .then((results) => {
            commit(types.FETCH_MYSELF, results)
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}
/**
 *
 * @param commit
 * @return {Promise<T | never>}
 */
export const fetchProjects = ({ commit,dispatch }) => {
    // API request
    return dispatch('fetchBacklogAPI', {url: 'projects', cache:true})
        .then((results) => {
            commit(types.FETCH_PROJECTS, results)
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}
/**
 *
 * @param commit
 * @param id
 * @return {Promise<T | never>}
 */
export const fetchProjectIssueTypes = ({ commit,dispatch },id) => {
    // API request
    return dispatch('fetchBacklogAPI',{ url:"projects/"+id+"/issueTypes", cache:true })
        .then((results) => { return results; })
        .catch((error) => { console.error(error) })
}
/**
 *
 * @param commit
 * @param id
 * @return {Promise<T | never>}
 */
export const fetchProjectCategories = ({ commit,dispatch },id) => {
    // API request
    return dispatch('fetchBacklogAPI',{ url:"projects/"+id+"/categories", cache:true })
        .then((results) => { return results; })
        .catch((error) => { console.error(error) })
}
/**
 *
 * @param commit
 * @param id
 * @return {Promise<T | never>}
 */
export const fetchProjectVersions = ({ commit,dispatch },id) => {
    // API request
    return dispatch('fetchBacklogAPI',{ url:"projects/"+id+"/versions", cache:true })
        .then((results) => { return results; })
        .catch((error) => { console.error(error) })
}
/**
 *
 * @param commit
 * @param id
 * @return {Promise<T | never>}
 */
export const fetchProjectMembers = ({ commit,dispatch },id) => {
    // API request
    return dispatch('fetchBacklogAPI',{ url:"projects/"+id+"/users", cache:true })
        .then((results) => {
            commit(types.FETCH_PROJECT_MEMBERS, { projectId:id, items:results })
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}

/**
 *
 * @see https://developer.nulab.com/ja/docs/backlog/api/2/get-issue-list/
 * @param commit
 * @param queryData
 * @return {Promise<T | never>}
 */
export const fetchIssues = ({ commit,dispatch },queryData) => {
    // API request
    let list = [];
    for( let k in queryData ){
        let v = queryData[k]
        if( !Array.isArray(v) )
            list.push(k+"="+v);
        else
            for( let item of v )
                list.push(k+"[]="+item);
    }

    return dispatch('fetchBacklogAPI',{ url:"issues", params:null, queryString:list.join('&') })
        .then((results) => {
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}

/**
 *
 * @param commit
 * @param id
 * @return {Promise<T | never>}
 */
export const fetchIssueComments = ({ commit,dispatch },id) => {
    // API request
    return dispatch('fetchBacklogAPI',{ url:"issues/"+id+"/comments", params:{  } })
        .then((results) => {
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}

/**
 *
 * @param commit
 * @param dispatch
 * @param id
 * @param queryData
 * @return {*|Promise<T | never>}
 */
export const updateIssue = ({ commit,dispatch }, {id, queryData} ) => {
    // API request
    return dispatch('fetchBacklogAPI',{
        url:"issues/"+id,
        // params: queryData,
        options:{
            method:'PATCH',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: Object.keys(queryData)
                .map((k)=>{
                    let v = queryData[k]
                    if( !Array.isArray(v) )
                        return (k+"="+v);
                    else {
                        let list = [];
                        for( let item of v )
                            list.push(k+"[]="+item);
                        return list.join('&')
                    }
                })
                .join('&')
        }
    })
        .then((results) => {
            return results;
        })
        .catch((error) => {
            console.error(error)
        })
}

/**
 *
 * @param commit
 * @return {*}
 */
export const fetchWorkspace = ({ commit }) => {
    // API request
    return commit(types.FETCH_WORKSPACE, JSON.parse( localStorage.getItem("workspace") || "{}" ) || {} );
}
/**
 *
 * @param commit
 * @param getters
 */
export const saveWorkspace = ({ commit,getters }) => {
    // API request
    localStorage.setItem("workspace", JSON.stringify( getters.workspace ) )
}

