import Vue from 'vue'
import Router from 'vue-router'
import Settings from 'components/Settings'
import Projects from 'components/Projects'
import Members from 'components/Members'
import KanbanView from 'components/KanbanView'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Settings',
            component: Settings
        },
        {
            path: '/projects',
            name: 'Projects',
            component: Projects
        },
        {
            path: '/members',
            name: 'Members',
            component: Members
        },
        {
            path: '/kanban',
            name: 'Kanban',
            component: KanbanView
        }
    ]
})
