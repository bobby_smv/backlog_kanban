import LineTalkMessageSender from 'components/line_talk/line-talk-message-sender';
import LineTalkMessageReceiver from 'components/line_talk/line-talk-message-receiver';

export default {
    components: {
        'message-sender': LineTalkMessageSender,
        'message-receiver': LineTalkMessageReceiver
    },
    props: {
        index: {
            type: Number,
            default: 0
        },
        item: {
            type: Object,
            default() {
                return {};
            }
        }
    },
    computed: {
        content() {
            let attrs = this.item.attributes || {};
            return attrs.content || {};
        },
        relationships() {
            return this.item.relationships;
        },
        useComponent() {
            return this.isSender ? 'message-sender' : 'message-receiver';
        },
        pictureUrl() {
            if (this.isSender) {
                // senderにアバタ画像つけるなら、画像パスを返す必要がある
                return undefined;
            } else {
                return this.$store.getters['user/pictureUrl'];;
            }
        },
        createdAt() {
            let defaultValues = {date: '', time: '00:00'};

            if (!this.relationships) {
                return defaultValues;
            }

            try {
                return this.relationships.created_at || defaultValues;
            } catch(error) {
                return defaultValues;
            }
        },
        isSender() {
            let defaultValue = false;

            if (!this.relationships) {
                return defaultValue;
            }

            try {
                return ('user' === this.relationships.sender) ? false : true;
            } catch(error) {
                return defaultValue;
            }
        }
    }

}
