import {getScrollY} from 'util.js';
import Alert from 'components/common/bootstrap-alert';

/**
 * setup:
 * vueインスタンスで、store module(form-validate)指定が必要
 */
export default {
    props: {
        formValidateModuleName: {
            type: String,
            default: 'form-validate'
        }
    },
    components: {
        'alert': Alert
    },
    computed: {
        /**
         * setup:
         * バリデーション対象値とバリデーションkeyを
         * vueインスタンス(orコンポーネント)で定義する
         *
         * @example: バリデーションkey "title" を this.titleの値でバリデート
         * return {title: this.title}
         *
         * @return  {object}
         */
        validateTargetValues() {
            return {};
        },

        isShowValidate: {
            /**
             * @return  {boolean}
             */
            get() {
                let module = this.formValidateModuleName;
                return this.$store.getters[`${module}/isShowValidate`];
            },
            /**
             * @param  {boolean}  value
             */
            set (value) {
                let module = this.formValidateModuleName;
                this.$store.dispatch(`${module}/updateIsShowValidate`, value);
                this.validate();
            }
        },
        /**
         * @return  {boolean}
         */
        isValid() {
            let module = this.formValidateModuleName;
            return this.$store.getters[`${module}/isValid`];
        },
        /**
         * @return  {boolean}
         */
        isSystemValid: {
            get() {
                let module = this.formValidateModuleName;
                return this.$store.getters[`${module}/isSystemValid`];
            },
            set(value) {
                let module = this.formValidateModuleName;
                return this.$store.dispatch(`${module}/updateIsSystemValid`, value);
            }
        },
        /**
         * @return  {array<object>} - [{title: true}, {hoge: false} ...]
         */
        isValids() {
            let module = this.formValidateModuleName;
            return this.$store.getters[`${module}/validStates`]('isValid');
        },
        /**
         * @return  {array<object>} - [{title: '必須入力です'}, {hoge: ''} ...]
         */
        validMessages() {
            let module = this.formValidateModuleName;
            return this.$store.getters[`${module}/validStates`]('message');
        },
        /**
         * @return  {array<object>} - [{title: ''}, {hoge: 'has-error'} ...]
         */
        validClasses() {
            let module = this.formValidateModuleName;
            return this.$store.getters[`${module}/validStates`]('validClass');
        },
        /**
         * @return  {array<string>}
         */
        validSystemMessages() {
            let module = this.formValidateModuleName;
            return this.$store.getters[`${module}/validSystemMessages`]();
        }
    },
    mounted() {
        let module = this.formValidateModuleName;
        this.$store.dispatch(`${module}/init`);

        if (this.isShowValidate) {
            this.validate();
        }
    },
    /**
     * setup:
     * vueインスタンス(orコンポーネント)で、バリデート対象モデルをwatchして
     * this.validateを実行させる
     */
    watch: {
        // title() {
        //     this.validate('title');
        // },
    },
    methods: {
        /**
         * @param  {string}  target - validate target key (not model value)
         */
        validate(target) {
            let module = this.formValidateModuleName;

            if (!!target) {
                // 単体バリデート
                this.$store.dispatch(`${module}/validate`, {
                    target,
                    value: this.validateTargetValues[target]
                });
                return;
            }
            // 全体バリデート
            this.$store.dispatch(`${module}/validateAll`, this.validateTargetValues);
        },

        onSubmit() {
            let form = this.$refs.form;

            this.isShowValidate = true;

            if (!this.isValid) {
                // バリデーションエラー
                this.scrollToAlert();
                return;
            }

            if (!!form) {
                form.submit();
                this.$root.$emit('SHOW_PAGE_LOADING');
            }
        },

        scrollToAlert(scrollElement) {
            let alert = this.$refs.alert;

            if (!alert) return;

            this.$nextTick(function() {
                let rect = alert.$el.getBoundingClientRect();
                let top  = rect.top || 0;

                if (top > -1) return;

                if (!scrollElement) {
                    window.scrollTo(0, getScrollY(window) + top);
                } else {
                    scrollElement.scrollTop = scrollElement.scrollTop + top;
                }
            });
        },

        // @return  {object}
        getValidateStateTemplate(...arg) {
            return this.$store.getters['form-validate/validateStateTemplate'].apply(null, arg);
        }
    }
}
