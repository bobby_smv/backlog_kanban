import MessageBox from 'components/message/message-edit-box';
import InputFileController from 'components/common/input-file-controller';

export default {
    components: {
        'input-file-controller': InputFileController,
        'message-box': MessageBox
    },
    props: {
        index: {
            type: Number,
            default: 0,
            required: true
        },
        name: {
            type: String,
            default: 'messages'
        },
        isShowValidate: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        disabled() {
            let canEdit = this.$store.getters['message-set/canEdit'];
            return !canEdit;
        },
        content() {
            return this.$store.getters['messages/content']('index', this.index);
        },
        order() {
            return this.$store.getters['messages/order']('index', this.index);
        },
        nameAttrPref() {
            return `${this.name}[${this.order}]`;
        },
        nameAttrType() {
            return this.nameAttrPref + '[msg_type]';
        },
        isShowValidate_() {
            return this.isShowValidate && !this.isValid;
        },
        validate() {
            return this.$store.getters['messages/validate/validateMessage']('index', this.index);
        },
        isValid() {
            return this.validate.hasOwnProperty('isValid') ? this.validate.isValid : true;
        },
        validMessage() {
            return this.validate.message || '';
        },
        validClass() {
            return this.isShowValidate_ ? 'has-error' : '';
        }
    },
}
