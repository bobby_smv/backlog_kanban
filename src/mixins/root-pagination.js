import {getAddQueryUrl} from 'util.js';
import Config from 'config.json';

export default {
    created() {
        this.pagination = this.pagination || {};
    },
    computed: {
        pagerTotal() {
            return this.pagination.total - 0 || 0;
        },
        pagerLimit() {
            return this.pagination.limit - 0 || 0;
        },
        pagerCurrentPage() {
            return this.pagination.current_page - 0 || 0;
        },
        pagerTotalPage() {
            return Math.ceil(this.pagerTotal / this.pagerLimit);
        },
        pagerRange() {
            let current = this.pagerCurrentPage;
            let limit = this.pagerLimit;
            let total = this.pagerTotal;

            let start = ((current - 1) * limit) + 1;
            let end   = Math.min(current * limit, total);

            return {start, end};
        }
    },
    methods: {
        /**
         * @param  {number}  page - index
         * @param  {number}  [limit]
         */
        onClickPagination(page, limit) {
            page = (typeof page === 'undefined') ? 1 : (page - 0 + 1);
            let queries = {page};
            if( limit ) queries.limit = limit;

            window.location.href = getAddQueryUrl(queries);

            this.$root.$emit('SHOW_PAGE_LOADING');
        }
    }
}
