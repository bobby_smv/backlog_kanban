import Modal from 'components/common/bootstrap-modal';

export default {
    components: {
        'modal': Modal
    },
    methods: {
        /**
         * @param  {bool}  isReplace - historyに残さない場合はtrue指定
         */
        onHideModal(isReplace) {
            let location = {name: 'home'};

            if (isReplace) {
                this.$router.replace(location);
            } else {
                this.$router.push(location);
            }
        }
    }
}
