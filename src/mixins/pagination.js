import Config from 'config.json';
import Pagination from 'components/common/bootstrap-pagination';

export default {
    components: {
        'pagination': Pagination
    },
    props: {
        // 表示するページ番号(not index)
        page: {
            type: [Number, String],
            default: 1
        },
        // 1ページあたりの件数
        lengthPerPage: {
            type: [Number, String],
            default: Config.pagination.length_per_page
        }
    },
    data() {
        return {
            pageCount: 0,
            currentPage: this.page - 0
        };
    },
    watch: {
        page(value) {
            this.currentPage = value - 0;
        }
    },
    // created() {
    //     let itemLength = 0;
    //     this.setPageCount(itemLength);
    // },
    methods: {

        /**
         * @param  {number}  pageIndex
         */
        onClickPagination(pageIndex) {
            if (!this.$router) {
                // not router
                this.currentPage = pageIndex + 1;
                return;
            }

            let current = this.$router.currentRoute;
            let options = {
                name : current.name,
                query: {page: pageIndex + 1, test:"aaa"}
            };

            this.$router.push(options);
            options = null;

            let modal = this.$refs.modal; //modal component

            if (!!modal && !!modal.$el) {
                modal.$el.scrollTop = 0;
            } else {
                window.scrollTo(0, 0);
            }
        },

        /**
         * @param  {number}  itemLength
         */
        setPageCount(itemLength) {
            this.pageCount = this.getPageCount(itemLength);
        },

        /**
         * @param  {number}  itemLength
         * @return {number}
         */
        getPageCount(itemLength) {
            return Math.ceil(itemLength / this.lengthPerPage);
        },

        /**
         *
         * @param items
         * @returns {array}
         */
        getPageItems(items) {
            if ( !(Array.isArray(items)) ) {
                return []; // not Array
            }
            let begin  = (this.page - 1) * this.lengthPerPage;
            let end = begin + this.lengthPerPage;

            return items.slice(begin , end);
        },

        /**
         *
         * @param index
         * @returns {*}
         */
        getItemIndex(index) {
            return (this.page - 1) * this.lengthPerPage + index;
        }
    }
}
